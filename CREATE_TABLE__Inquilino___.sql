CREATE TABLE [Inquilino] (
  [IdInquilino] int,
  [NombreInquilino] varchar(100),
  [ApellidoInquilino] varchar(100),
  [TelefonoInquilino] varchar(15),
  PRIMARY KEY ([IdInquilino])
);

CREATE TABLE [Propietario] (
  [IdPropietario] number,
  [NombrePropietario] varchar(100),
  [ApellidoPropietario] varchar(100),
  [TelefonoPropietario] varchar(15),
  PRIMARY KEY ([IdPropietario])
);

CREATE TABLE [Barrio] (
  [IdBarrio] int,
  [NombreBarrio] varchar(50),
  PRIMARY KEY ([IdBarrio])
);

CREATE TABLE [Propiedad] (
  [IdPropiedad] int,
  [CallePropiedad] varchar(100),
  [NúmeroPropiedad] varchar(100),
  [IdBarrio] varchar(50),
  [CostoAlquiler] int,
  [IdInmobiliaria] varchar(50),
  [Pais] varchar(50),
  [Estado] varchar(50),
  PRIMARY KEY ([IdPropiedad]),
  CONSTRAINT [FK_Propiedad.IdBarrio]
    FOREIGN KEY ([IdBarrio])
      REFERENCES [Barrio]([IdBarrio])
);

CREATE TABLE [PropiedadPropietario] (
  [IdRegistro] int,
  [IdPropietario] int,
  [IdPropiedad] int,
  PRIMARY KEY ([IdRegistro])
);
